package com.manage.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manage.employee.model.Penempatan;

@Repository
public interface PenempatanRepository extends JpaRepository<Penempatan, Long>{

}
