package com.manage.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.manage.employee.model.Lemburdanbonus;

@Repository
public interface LemburdanbonusRepository extends JpaRepository<Lemburdanbonus, Long>{

}
