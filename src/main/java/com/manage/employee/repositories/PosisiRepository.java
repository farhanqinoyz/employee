package com.manage.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manage.employee.model.Posisi;

@Repository
public interface PosisiRepository extends JpaRepository<Posisi, Long>{

}
