package com.manage.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manage.employee.model.Parameter;

@Repository
public interface ParameterRepository extends JpaRepository<Parameter, Long>{

}
