package com.manage.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manage.employee.model.TunjanganPegawai;

@Repository
public interface TunjanganPegawaiRepository extends JpaRepository<TunjanganPegawai, Long>{

}
