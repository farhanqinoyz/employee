package com.manage.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.manage.employee.model.Kemampuan;

@Repository
public interface KemampuanRepository extends JpaRepository<Kemampuan,Long>{

}
