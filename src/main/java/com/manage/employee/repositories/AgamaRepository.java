package com.manage.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.manage.employee.model.Agama;

@Repository
public interface AgamaRepository extends JpaRepository<Agama, Long>{

}
