package com.manage.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manage.employee.model.PresentaseGaji;
@Repository
public interface PersentasiGajiRepository extends JpaRepository<PresentaseGaji, Long>{
	
}
