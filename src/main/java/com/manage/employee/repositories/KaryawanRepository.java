package com.manage.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.manage.employee.model.Karyawan;

@Repository
public interface KaryawanRepository extends JpaRepository<Karyawan,Long>{

}
