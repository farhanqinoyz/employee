package com.manage.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.manage.employee.model.KategoriKemampuan;

@Repository
public interface KategoriKemampuanRepository extends JpaRepository<KategoriKemampuan,Long>{

}
