package com.manage.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manage.employee.model.Tingkatan;

@Repository
public interface TingkatanRepository extends JpaRepository<Tingkatan, Long>{

}
