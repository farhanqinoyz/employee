package com.manage.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.manage.employee.model.Pendapatan;

@Repository
public interface PendapatanRepository extends JpaRepository<Pendapatan, Long> {

}
