package com.manage.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manage.employee.dtomodel.PendapatanDTO;
import com.manage.employee.model.Pendapatan;
import com.manage.employee.repositories.PendapatanRepository;
@RestController
@RequestMapping("api/pendapatan")
public class PendapatanController {
	@Autowired
	PendapatanRepository pendapatanRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllPendapatan(){
		
		ModelMapper modelMapper = new ModelMapper();
		Map<String, Object> result = new HashMap<String, Object>();
		List<Pendapatan> listPendapatan= pendapatanRepository.findAll();
		List<PendapatanDTO> listPendapatanDTO = new ArrayList();
		
		for(Pendapatan pendapatan : listPendapatan) {
			PendapatanDTO dto = modelMapper.map(pendapatan, PendapatanDTO.class);
			listPendapatanDTO.add(dto);
		}
			
		result.put("Status", 200);
		result.put("Message", "Read data Pendapatan success!");
		result.put("Data", listPendapatanDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreatePendapatan(@Valid @RequestBody PendapatanDTO pendapatanDTO){
		ModelMapper modelMapper = new ModelMapper();
			Pendapatan pendapatan = modelMapper.map(pendapatanDTO, Pendapatan.class);
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Create data Pendapatan success!");
		result.put("Data", pendapatanRepository.save(pendapatan));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updatePendapatan(@Valid @RequestBody PendapatanDTO pendapatanDTO,
			@PathVariable(value = "id") Long idPendapatan){

		Map<String, Object> result = new HashMap<String, Object>(); 
		ModelMapper modelMapper = new ModelMapper();
		Pendapatan pendapatanById = pendapatanRepository.findById(idPendapatan).get();
		pendapatanById = modelMapper.map(pendapatanDTO,Pendapatan.class);
		
		pendapatanById.setIdPendapatan(idPendapatan);
		
		pendapatanRepository.save(pendapatanById);
		result.put("Status", 200);
		result.put("Message", "Update data Pendapatan success!");
		result.put("Data", pendapatanById);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePublisherById(@PathVariable(value = "id") Long idPendapatan) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Pendapatan pendapatan = pendapatanRepository.findById(idPendapatan).get();
    	
        pendapatanRepository.delete(pendapatan);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record category success!");
		result.put("ID : ",""+idPendapatan);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
}
