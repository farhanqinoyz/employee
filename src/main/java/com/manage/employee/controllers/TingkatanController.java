package com.manage.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manage.employee.dtomodel.TingkatanDTO;
import com.manage.employee.model.Tingkatan;
import com.manage.employee.repositories.TingkatanRepository;
@RestController
@RequestMapping("api/tingkatan")
public class TingkatanController {
	@Autowired
	TingkatanRepository tingkatanRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllTingkatan(){
		
		ModelMapper modelMapper = new ModelMapper();
		Map<String, Object> result = new HashMap<String, Object>();
		List<Tingkatan> listTingkatan= tingkatanRepository.findAll();
		List<TingkatanDTO> listTingkatanDTO = new ArrayList();
		
		for(Tingkatan tingkatan : listTingkatan) {
			TingkatanDTO dto = modelMapper.map(tingkatan, TingkatanDTO.class);
			listTingkatanDTO.add(dto);
		}
			
		result.put("Status", 200);
		result.put("Message", "Read data Tingkatan success!");
		result.put("Data", listTingkatanDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreateTingkatan(@Valid @RequestBody TingkatanDTO tingkatanDTO){
		ModelMapper modelMapper = new ModelMapper();
			Tingkatan tingkatan = modelMapper.map(tingkatanDTO, Tingkatan.class);
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Create data Tingkatan success!");
		result.put("Data", tingkatanRepository.save(tingkatan));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateTingkatan(@Valid @RequestBody TingkatanDTO tingkatanDTO,
			@PathVariable(value = "id") Long idTingkatan){

		Map<String, Object> result = new HashMap<String, Object>(); 
		ModelMapper modelMapper = new ModelMapper();
		Tingkatan tingkatanById = tingkatanRepository.findById(idTingkatan).get();
		tingkatanById = modelMapper.map(tingkatanDTO,Tingkatan.class);
		
		tingkatanById.setIdTingkatan(idTingkatan);
		
		tingkatanRepository.save(tingkatanById);
		result.put("Status", 200);
		result.put("Message", "Update data Tingkatan success!");
		result.put("Data", tingkatanById);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePublisherById(@PathVariable(value = "id") Long idTingkatan) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Tingkatan tingkatan = tingkatanRepository.findById(idTingkatan).get();
    	
        tingkatanRepository.delete(tingkatan);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record category success!");
		result.put("ID : ",""+idTingkatan);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
}
