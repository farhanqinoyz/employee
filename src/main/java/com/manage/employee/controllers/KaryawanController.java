package com.manage.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manage.employee.dtomodel.KaryawanDTO;
import com.manage.employee.model.Karyawan;
import com.manage.employee.repositories.KaryawanRepository;

@RestController
@RequestMapping("api/karyawan")
public class KaryawanController {
	@Autowired
	KaryawanRepository karyawanRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllKaryawan(){
		ModelMapper modelMapper = new ModelMapper();
		List<Karyawan> listKaryawan = karyawanRepository.findAll();
		List<KaryawanDTO> listKaryawanDTO = new ArrayList();
		
		for(Karyawan karyawan : listKaryawan) {
			KaryawanDTO dto = modelMapper.map(karyawan, KaryawanDTO.class);
			listKaryawanDTO.add(dto);
		}
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		
		result.put("Status", 200);
		result.put("Message", "Read List Karyawan success!");
		result.put("Data", listKaryawanDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreateKaryawan(@Valid @RequestBody KaryawanDTO karyawanDTO){
		ModelMapper modelMapper = new ModelMapper();
			Karyawan karyawan = modelMapper.map(karyawanDTO, Karyawan.class);
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Create data Karyawan success!");
		result.put("Data", karyawanRepository.save(karyawan));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateKaryawan(@Valid @RequestBody KaryawanDTO karyawanDTO,
			@PathVariable(value = "id") Long idKaryawan){

		Map<String, Object> result = new HashMap<String, Object>(); 
		ModelMapper modelMapper = new ModelMapper();
		Karyawan karyawanById = karyawanRepository.findById(idKaryawan).get();
		karyawanById = modelMapper.map(karyawanDTO,Karyawan.class);
		
		karyawanById.setIdKaryawan(idKaryawan);
		
		karyawanRepository.save(karyawanById);
		result.put("Status", 200);
		result.put("Message", "Update data Agama success!");
		result.put("Data", karyawanById);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePublisherById(@PathVariable(value = "id") Long idKaryawan) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Karyawan karyawan = karyawanRepository.findById(idKaryawan).get();
    	
        karyawanRepository.delete(karyawan);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record category success!");
		result.put("ID : ",""+idKaryawan);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
}
