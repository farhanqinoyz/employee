package com.manage.employee.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manage.employee.dtomodel.KaryawanDTO;
import com.manage.employee.dtomodel.LemburdanbonusDTO;
import com.manage.employee.dtomodel.ParameterDTO;
import com.manage.employee.dtomodel.PendapatanDTO;
import com.manage.employee.dtomodel.PosisiDTO;
import com.manage.employee.dtomodel.PresentaseGajiDTO;
import com.manage.employee.model.Karyawan;
import com.manage.employee.model.Lemburdanbonus;
import com.manage.employee.model.Parameter;
import com.manage.employee.model.Pendapatan;
import com.manage.employee.model.Posisi;
import com.manage.employee.model.PresentaseGaji;
import com.manage.employee.model.TunjanganPegawai;
import com.manage.employee.repositories.KaryawanRepository;
import com.manage.employee.repositories.LemburdanbonusRepository;
import com.manage.employee.repositories.ParameterRepository;
import com.manage.employee.repositories.PendapatanRepository;
import com.manage.employee.repositories.PersentasiGajiRepository;
import com.manage.employee.repositories.PosisiRepository;
import com.manage.employee.repositories.TunjanganPegawaiRepository;

@RestController
@RequestMapping("api/calculate") 
public class CalculateSalaryController {
	
	//Mengambil Repository dari Class masing-masing
	@Autowired
	KaryawanRepository karyawanRepository;

	@Autowired
	PendapatanRepository pendapatanRepository;
	
	@Autowired
	PersentasiGajiRepository presentaseGajiRepository;
	
	@Autowired
	ParameterRepository parameterRepository;
	
	@Autowired
	TunjanganPegawaiRepository tunjanganPegawaiRepository;
	
	@Autowired
	LemburdanbonusRepository lemburdanbonusRepository;
	
	@Autowired
	PosisiRepository posisiRepository;
	
	@GetMapping("/salary/{date}") // Tambahkan param tanggal 
	public Map<String, Object> getAllSalary(@PathVariable(value = "date")  
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateParameter){
		Calendar calendarParameter = Calendar.getInstance();
		calendarParameter.setTime(dateParameter);
		int yearParameter = calendarParameter.get(Calendar.YEAR);
		int monthParameter = calendarParameter.get(Calendar.MONTH) + 1;
		
		
		
		ModelMapper modelMapper = new ModelMapper();
		
		//Deklarasi list dan otomatis mengambil seluruh Data agar bisa langsung diubah menjadi DTO
		List<Karyawan> listKaryawan = karyawanRepository.findAll();
		List<Parameter> listParameter = parameterRepository.findAll();
		List<KaryawanDTO> listKaryawanDTO = new ArrayList<KaryawanDTO>();
		List<ParameterDTO> listParameterDTO = new ArrayList<ParameterDTO>();
		List<PendapatanDTO> listPendapatanDTO = mappingPendapatan();
		List<PendapatanDTO> listPendapatanAkhir = new ArrayList<PendapatanDTO>();
		
		//Inisialisasi untuk tabel Parameter
		BigDecimal issurance = new BigDecimal(0);
		BigDecimal transportBonus = new BigDecimal(0);
		BigDecimal familyBonus = new BigDecimal(0);
		BigDecimal overTimeBonus = new BigDecimal(0);
		
		//Mengambil Isi Value dari Tabel Parameter
		for(Parameter parameter : listParameter) {
			ParameterDTO dto = modelMapper.map(parameter, ParameterDTO.class);
			listParameterDTO.add(dto);
		}
		for(ParameterDTO parameter : listParameterDTO) {
			issurance = parameter.getPBpjs();
			transportBonus = parameter.getTTransport();
			familyBonus = parameter.getTKeluarga();
			overTimeBonus = parameter.getLembur();
			}
		
		for(Karyawan karyawan : listKaryawan) {
			KaryawanDTO dto = modelMapper.map(karyawan, KaryawanDTO.class);
			listKaryawanDTO.add(dto);
		}
		//Mengambil seluruh Data karyawan
		for(KaryawanDTO karyawan : listKaryawanDTO) { 
			
			//Memanggil berbagai fungsi untuk perhitungan pendapatan
			BigDecimal employeeSalaryBasic = getGapokByIdKaryawan(karyawan); 
			BigDecimal employeeIssurance = getIssuranceKaryawan(employeeSalaryBasic, issurance);
			BigDecimal transportationBonus = getTransportBonus(karyawan, transportBonus);
			BigDecimal employeeBonus = getEmployeeBonus(karyawan);
			BigDecimal employeeFamilyBonus = getFamilyBonus(karyawan, familyBonus, employeeSalaryBasic);
			BigDecimal grossSalary = employeeSalaryBasic.add(transportationBonus).add(employeeBonus).add(employeeFamilyBonus);
			BigDecimal netSalary = grossSalary.subtract(employeeIssurance);
			int overTimeDuration = getLamaLemburKaryawan(karyawan);
			BigDecimal overTimeBonusEmployee = countOverTime(overTimeBonus, overTimeDuration, netSalary);
			BigDecimal moneyBonus = countBonus(karyawan, dateParameter);
			BigDecimal takeHomePay = getTakeHomePay(netSalary, overTimeBonusEmployee,moneyBonus);
			//Mengisi Class ini untuk dimasukkan ke tabel 
			PendapatanDTO pendapatanDTO = new PendapatanDTO();
			pendapatanDTO.setKaryawan(karyawan);
			pendapatanDTO.setGajiPokok(employeeSalaryBasic);
			pendapatanDTO.setBpjs(employeeIssurance);
			pendapatanDTO.setTunjanganTransport(transportationBonus);
			pendapatanDTO.setTunjanganKeluarga(employeeFamilyBonus);
			pendapatanDTO.setTunjanganPegawai(employeeBonus);
			pendapatanDTO.setGajiBersih(netSalary);
			pendapatanDTO.setGajiKotor(grossSalary);
			pendapatanDTO.setPphPerbulan(BigDecimal.valueOf(0));
			pendapatanDTO.setUangLembur(overTimeBonus);
			pendapatanDTO.setVariableBonus(0);
			pendapatanDTO.setUangBonus(moneyBonus);
			pendapatanDTO.setTakeHomePay(takeHomePay);
			pendapatanDTO.setTanggalGaji(dateParameter);
			pendapatanDTO.setLamaLembur(overTimeDuration);
			listPendapatanAkhir.add(pendapatanDTO);
			Pendapatan pendampatanUpdate = modelMapper.map(pendapatanDTO, Pendapatan.class);
			
			//Mendeklarasi dan menginisilisasi variable untuk pengkondisian
			Boolean toAdd = false;
			Boolean toUpdate = false;
			Boolean isNotExist = true;
			
			
			//Mengeluarkan seluruh isi Record yang ada pada table pendapatan
			for(PendapatanDTO pendapatan : listPendapatanDTO) {
				Long idEmployeeSalary = pendapatan.getKaryawan().getIdKaryawan();
				
				//Mengecek apakah id karyawan sudah ada di database
				if(karyawan.getIdKaryawan() == idEmployeeSalary) {
					
					//Mengubah parameter yang dimasukan pada parameter API menjadi variable integer
					int yearRequest = yearParameter;
					int monthRequest = monthParameter;
					
					//Mengambil Tanggal dari Database
					Date dateSalary = pendapatan.getTanggalGaji();
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(dateSalary);
					
					int yearPendapatan = calendar.get(Calendar.YEAR); // Mengambil tahun
					int monthPendapatan = calendar.get(Calendar.MONTH) + 1; 
					//Mengambil bulan, ditambah satu karena Januari itu 0(seperti array)
					
						if(yearRequest == yearPendapatan) { //Apabila ternyata tahun nya sama
							if(monthRequest == monthPendapatan){
								toUpdate = true; // IdPendapatan akan dihitung ulang dan diupdate
								isNotExist = false;
								toAdd = false;
								
							}else if(monthRequest - monthPendapatan >= 1) {
								toAdd = true; // Akan ditambahkan
								isNotExist = false;
								toUpdate = false;
							}
						}else if(monthRequest == monthPendapatan){
							toAdd = true;
							isNotExist = false;
							toUpdate = false;
						}
				}
			}
			
			if(toAdd || isNotExist) { //Kalau ternyata setelah di cek, belum ada 
				pendapatanRepository.save(pendampatanUpdate); //Akan menambahkan sebuah record baru
			}else if(toUpdate == true) {
				List<PendapatanDTO> list = mappingPendapatan();
				Long idPendapatan = null;
				
				for(PendapatanDTO itemPendapatan : list) {
					Long idKaryawanPendapatan = itemPendapatan.getKaryawan().getIdKaryawan();
					
					
					if(idKaryawanPendapatan == karyawan.getIdKaryawan()) {
						idPendapatan = itemPendapatan.getIdPendapatan();
					}
				}
				
				Pendapatan pendapatan = pendapatanRepository.findById(idPendapatan).get();

				Date dateDatabase = pendapatan.getTanggalGaji();
				PendapatanDTO pendapatanUpdate = modelMapper.map(pendapatan, PendapatanDTO.class);
				
				pendapatanUpdate.setKaryawan(karyawan);
				pendapatanUpdate.setGajiPokok(employeeSalaryBasic);
				pendapatanUpdate.setBpjs(employeeIssurance);
				pendapatanUpdate.setTunjanganTransport(transportationBonus);
				pendapatanUpdate.setTunjanganKeluarga(employeeFamilyBonus);
				pendapatanUpdate.setTunjanganPegawai(employeeBonus);
				pendapatanUpdate.setGajiBersih(netSalary);
				pendapatanUpdate.setGajiKotor(grossSalary);
				pendapatanUpdate.setPphPerbulan(BigDecimal.valueOf(0));
				pendapatanUpdate.setLamaLembur(overTimeDuration);
				pendapatanUpdate.setUangLembur(overTimeBonusEmployee);
				pendapatanUpdate.setVariableBonus(0);
				pendapatanUpdate.setUangBonus(moneyBonus);
				pendapatanUpdate.setTakeHomePay(takeHomePay);
				pendapatanUpdate.setTanggalGaji(dateDatabase);


				Pendapatan pendampatanUpdateTable = modelMapper.map(pendapatanUpdate, Pendapatan.class);
				pendapatanRepository.save(pendampatanUpdateTable);
				listPendapatanAkhir.add(pendapatanUpdate);
				}
		
		}
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		
		result.put("Status", 200);
		result.put("Message", "Update seluruh Salary Sukses!");
		result.put("Data", listPendapatanAkhir);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	   
		}
		return result;
	}
	
	BigDecimal countBonus(KaryawanDTO karyawan, Date dateSalary) {
		ModelMapper modelMapper = new ModelMapper();
		List<Posisi> listPosisi = posisiRepository.findAll(Sort.by(Sort.Direction.ASC, "namaPosisi"));
		
		List<PosisiDTO> listPosisiDTO = new ArrayList<PosisiDTO>();
		for(Posisi posisi : listPosisi) {
			PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
			listPosisiDTO.add(posisiDTO);
		}
		BigDecimal countBonus = BigDecimal.ZERO;
		List<String> listPositionName = new ArrayList<String>();
		String positionName = karyawan.getPosisi().getNamaPosisi();
		List<LemburdanbonusDTO> listLemburdanbonus = MappingLemburdanbonus();
		List<Parameter> listParameter = parameterRepository.findAll();
		List<ParameterDTO> listParameterDTO = new ArrayList<ParameterDTO>();
		BigDecimal multipleBonus = BigDecimal.ZERO;
		Integer maxBonus = 0;
		Integer variableBonus = 0;
		BigDecimal maxBonusTotal = BigDecimal.ZERO;
		for(Parameter parameter : listParameter) {
			ParameterDTO dto = modelMapper.map(parameter, ParameterDTO.class);
			listParameterDTO.add(dto);
		}
		for(ParameterDTO parameter : listParameterDTO) {
			if(karyawan.getPosisi().getNamaPosisi().equalsIgnoreCase(listPosisiDTO.get(1).getNamaPosisi())) {
				multipleBonus = parameter.getBonusPg();
				maxBonus = parameter.getBatasanBonusPg();
			}else if(karyawan.getPosisi().getNamaPosisi().equalsIgnoreCase(listPosisiDTO.get(2).getNamaPosisi())) {
				multipleBonus = parameter.getBonusTw();
				maxBonus = parameter.getBatasanBonusTw();
			}else if(karyawan.getPosisi().getNamaPosisi().equalsIgnoreCase(listPosisiDTO.get(3).getNamaPosisi())) {
				multipleBonus = parameter.getBonusTs();
				maxBonus = parameter.getBatasanBonusTs();
			}
			maxBonusTotal = parameter.getMaxBonus();
		}
		for(LemburdanbonusDTO lemburdanbonusDTO : listLemburdanbonus) {
			Date dateOverTime = lemburdanbonusDTO.getTanggal();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateOverTime);
			
			int yearLembur = calendar.get(Calendar.YEAR); // Mengambil tahun
			int monthLembur = calendar.get(Calendar.MONTH) + 1; 
			
			Calendar calendarSalary = Calendar.getInstance();
			calendarSalary.setTime(dateSalary);
			
			int yearSalary = calendarSalary.get(Calendar.YEAR); // Mengambil tahun
			int monthSalary = calendarSalary.get(Calendar.MONTH) + 1; 
			if(lemburdanbonusDTO.getKaryawan().getIdKaryawan() == karyawan.getIdKaryawan()
					&& yearLembur == yearSalary && monthLembur == monthSalary) {
				variableBonus = lemburdanbonusDTO.getVariableBonus();

			}
			
		}
		
		countBonus = multipleBonus.multiply(BigDecimal.valueOf(variableBonus)).multiply(BigDecimal.valueOf(maxBonus));
		System.out.println("Count : "+countBonus);
		System.out.println("Multiple : "+multipleBonus);
		System.out.println("Variable : "+variableBonus);
		System.out.println("Max : "+maxBonus);
		
		if(countBonus.compareTo(maxBonusTotal) > 0) {
			countBonus = maxBonusTotal;
		}
		return countBonus;
	}
	
	BigDecimal countOverTime(BigDecimal overTimeBonus, int overTimeDuration, BigDecimal netSalary) {	
		BigDecimal overTimeTotal = overTimeBonus.multiply(BigDecimal.valueOf(overTimeDuration).multiply(netSalary));
		return overTimeTotal;
	}
	
	public BigDecimal getTakeHomePay(BigDecimal netSalary, BigDecimal overTimeBonusEmployee, BigDecimal moneyBonus) {
		BigDecimal takeHomePay = netSalary.add(overTimeBonusEmployee).add(moneyBonus);
		
		return takeHomePay;
	}
	
	public BigDecimal getFamilyBonus(KaryawanDTO karyawan, BigDecimal familyBonus,BigDecimal gajiPokokKaryawan){
		BigDecimal tempFamilyBonus = new BigDecimal(0);
		if(karyawan.getStatusPernikahan() == 1) {
			tempFamilyBonus = familyBonus.multiply(gajiPokokKaryawan);	
		}
		
		return tempFamilyBonus;
	}
	
	public BigDecimal getEmployeeBonus(KaryawanDTO karyawanDTO) { // Ambil dari Karyawan
		ModelMapper modelMapper = new ModelMapper();
		Karyawan karyawan = modelMapper.map(karyawanDTO, Karyawan.class);
		Long idPosisi = karyawan.getPosisi().getIdPosisi();
		Long idTingkatan = karyawan.getTingkatan().getIdTingkatan();
		List<TunjanganPegawai> listTunjanganPegawai = tunjanganPegawaiRepository.findAll();
		BigDecimal besaranTunjangan = new BigDecimal(0);
		for(TunjanganPegawai tunjanganPegawai: listTunjanganPegawai) {
			
			if(tunjanganPegawai.getPosisi().getIdPosisi() == idPosisi && tunjanganPegawai.getTingkatan().getIdTingkatan() == idTingkatan) {
				besaranTunjangan = tunjanganPegawai.getBesaranTujnaganPegawai();
			}
			
		}
		return besaranTunjangan;
		
	}
	public BigDecimal getTransportBonus(KaryawanDTO karyawan, BigDecimal transportBonus){
		BigDecimal tempTransportBonus = new BigDecimal(0);
		if(karyawan.getPenempatan().getKotaPenempatan().equalsIgnoreCase("DKI Jakarta")) {
			tempTransportBonus = transportBonus;	
		}
		
		return tempTransportBonus;
	}
	
	public BigDecimal getIssuranceKaryawan(BigDecimal gajiPokokKaryawan, BigDecimal issurance){
		BigDecimal tempIssuranceFund = gajiPokokKaryawan.multiply(issurance);
		
		return tempIssuranceFund ;
	}
	
	public List<PresentaseGajiDTO> mappingPresentaseGaji() {
		ModelMapper modelMapper = new ModelMapper();
		
		List<PresentaseGaji> listPresentaseGaji = presentaseGajiRepository.findAll();
		List<PresentaseGajiDTO> listPresentaseGajiDTO = new ArrayList<PresentaseGajiDTO>();
		for(PresentaseGaji presentaseGaji : listPresentaseGaji) {
			PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
			listPresentaseGajiDTO.add(presentaseGajiDTO);
		}
		
		return listPresentaseGajiDTO;
	}
	
	public List<PosisiDTO> MappingPosisi(){
		ModelMapper modelMapper = new ModelMapper();
		
		List<Posisi> listPosisi = posisiRepository.findAll();
		List<PosisiDTO> listPosisiDTO = new ArrayList<PosisiDTO>();
		for(Posisi posisi : listPosisi) {
			PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
			listPosisiDTO.add(posisiDTO);
		}
		return listPosisiDTO;
	}
	
	public List<LemburdanbonusDTO> MappingLemburdanbonus(){
		ModelMapper modelMapper = new ModelMapper();
		
		List<Lemburdanbonus> listLemburdanbonus= lemburdanbonusRepository.findAll();
		List<LemburdanbonusDTO> listLemburdanbonusDTO = new ArrayList<LemburdanbonusDTO>();
		for(Lemburdanbonus lemburdanbonus : listLemburdanbonus) {
			LemburdanbonusDTO lemburdanbonusDTO = modelMapper.map(lemburdanbonus, LemburdanbonusDTO.class);
			listLemburdanbonusDTO.add(lemburdanbonusDTO);
		}
		return listLemburdanbonusDTO;
	}

	public List<PendapatanDTO> mappingPendapatan() {
		ModelMapper modelMapper = new ModelMapper();
		
		List<Pendapatan> listPendapatan= pendapatanRepository.findAll();
		List<PendapatanDTO> listPendapatanDTO = new ArrayList<PendapatanDTO>();
		for(Pendapatan pendapatan : listPendapatan) {
			PendapatanDTO pendapatanDTO = modelMapper.map(pendapatan, PendapatanDTO.class);
			listPendapatanDTO.add(pendapatanDTO);
		}
		
		return listPendapatanDTO;
	}
	
	public BigDecimal getPercentSalary(KaryawanDTO karyawan) {
		List<PresentaseGajiDTO> listPresentaseGaji = mappingPresentaseGaji();
		List<PresentaseGajiDTO> listPresentase = new ArrayList<PresentaseGajiDTO>();
		BigDecimal percentSalary = new BigDecimal(0);
		for(PresentaseGajiDTO presentaseGaji : listPresentaseGaji) {
			Boolean isPosisi = presentaseGaji.getPosisi().getIdPosisi() == karyawan.getPosisi().getIdPosisi();
			Boolean isTingkatan = presentaseGaji.getTingkatan().getIdTingkatan() == karyawan.getTingkatan().getIdTingkatan();
			
			if(isPosisi && isTingkatan) {
				listPresentase.add(presentaseGaji);
			}
		}
		PresentaseGajiDTO presentaseGajiDTO = Collections.max(listPresentase, Comparator.comparing(s -> s.getMasaKerja()));	
		for(PresentaseGajiDTO presentaseGaji : listPresentase) {
			Boolean isPosisi = presentaseGaji.getPosisi().getIdPosisi() == karyawan.getPosisi().getIdPosisi();
			Boolean isMasaKerja = karyawan.getMasaKerja() == presentaseGaji.getMasaKerja();
			Boolean isTingkatan = presentaseGaji.getTingkatan().getIdTingkatan() == karyawan.getTingkatan().getIdTingkatan();
			
			if(isPosisi && isMasaKerja && isTingkatan) {
								percentSalary = presentaseGaji.getBesaranGaji();
			}else if(isPosisi && isTingkatan && presentaseGajiDTO.getMasaKerja() < karyawan.getMasaKerja() && presentaseGajiDTO.getMasaKerja() == presentaseGaji.getMasaKerja()) {
				percentSalary = presentaseGajiDTO.getBesaranGaji();			
			}
		}
		
		return percentSalary;
	}
	
	
	
	public Integer getLamaLemburKaryawan(KaryawanDTO karyawan) {
		
		List<PendapatanDTO> listPendapatanDTO = mappingPendapatan();
		List<PendapatanDTO> listPendapatanDTOSalary = new ArrayList<PendapatanDTO>();
		List<LemburdanbonusDTO> listLemburdanbonus = MappingLemburdanbonus();
		Date dateSalary = new Date();

		Long idKaryawan = karyawan.getIdKaryawan();
		for(PendapatanDTO pendapatan : listPendapatanDTO) {
			
			if(idKaryawan==pendapatan.getKaryawan().getIdKaryawan()) {
				listPendapatanDTOSalary.add(pendapatan);
			}		
		}
		int overTimeDuration = 0;
		for(PendapatanDTO pendapatan : listPendapatanDTOSalary) {

			dateSalary = pendapatan.getTanggalGaji();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateSalary);
			
			int yearPendapatan = calendar.get(Calendar.YEAR); // Mengambil tahun
			int monthPendapatan = calendar.get(Calendar.MONTH) + 1; 

		for(LemburdanbonusDTO lemburdanbonus : listLemburdanbonus) {
			Long idKaryawanLembur = lemburdanbonus.getKaryawan().getIdKaryawan();
			Date tanggalLembur = lemburdanbonus.getTanggal();
			Calendar calendarLembur = Calendar.getInstance();
			calendarLembur.setTime(tanggalLembur);
			
			int yearLembur = calendarLembur.get(Calendar.YEAR); // Mengambil tahun
			int monthLembur = calendarLembur.get(Calendar.MONTH) + 1; 

			if(idKaryawan == idKaryawanLembur) {
				if(yearPendapatan == yearLembur) {
					if(monthPendapatan == monthLembur) {
						overTimeDuration = lemburdanbonus.getLamaLembur();
					}
				}else if(monthLembur == monthPendapatan){
					overTimeDuration = lemburdanbonus.getLamaLembur();
				}
			}
			
		}
		}
		
		
		return overTimeDuration;
	}

	public BigDecimal getGapokByIdKaryawan(KaryawanDTO karyawan){			
			BigDecimal percent = getPercentSalary(karyawan);
			BigDecimal tempResult = new BigDecimal(0);
			tempResult = new BigDecimal(0);	
			tempResult = percent.multiply(karyawan.getPenempatan().getUmkPenempatan());
			
		return tempResult;
	}
}