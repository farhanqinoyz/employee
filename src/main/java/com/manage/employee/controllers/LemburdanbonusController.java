package com.manage.employee.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manage.employee.dtomodel.LemburdanbonusDTO;
import com.manage.employee.dtomodel.PendapatanDTO;
import com.manage.employee.model.Lemburdanbonus;
import com.manage.employee.model.Pendapatan;
import com.manage.employee.repositories.LemburdanbonusRepository;
import com.manage.employee.repositories.PendapatanRepository;
@RestController
@RequestMapping("api/lemburdanbonus")
public class LemburdanbonusController {
	@Autowired
	LemburdanbonusRepository lemburdanbonusRepository;
	
	@Autowired
	PendapatanRepository pendapatanRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllLemburdanbonus(){
		
		ModelMapper modelMapper = new ModelMapper();
		Map<String, Object> result = new HashMap<String, Object>();
		List<Lemburdanbonus> listLemburdanbonus= lemburdanbonusRepository.findAll();
		List<LemburdanbonusDTO> listLemburdanbonusDTO = new ArrayList<LemburdanbonusDTO>();
		
		for(Lemburdanbonus lemburdanbonus : listLemburdanbonus) {
			LemburdanbonusDTO dto = modelMapper.map(lemburdanbonus, LemburdanbonusDTO.class);
			listLemburdanbonusDTO.add(dto);
		}
			
		result.put("Status", 200);
		result.put("Message", "Read data Lemburdanbonus success!");
		result.put("Data", listLemburdanbonusDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	 
	@PostMapping("/create")
	public Map<String, Object> CreateLemburdanbonus(@Valid @RequestBody LemburdanbonusDTO lemburdanbonusDTO){
		ModelMapper modelMapper = new ModelMapper();
		Lemburdanbonus lemburdanbonus = modelMapper.map(lemburdanbonusDTO, Lemburdanbonus.class);
		List<LemburdanbonusDTO> listLemburdanbonusDTO = MappingLemburdanbonus();
		Boolean isExist = false;
		Map<String, Object> result = new HashMap<String, Object>();
		Lemburdanbonus saveLemburdanbonus = new Lemburdanbonus();
		
		for(LemburdanbonusDTO lemburdanbonusLoop : listLemburdanbonusDTO) {
			Date tanggalLembur = lemburdanbonusLoop.getTanggal();
			Calendar calendarLembur = Calendar.getInstance();
			calendarLembur.setTime(tanggalLembur);
			
			Date dateSalary = lemburdanbonusDTO.getTanggal();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateSalary);
			
			int yearLembur = calendarLembur.get(Calendar.YEAR); // Mengambil tahun
			int monthLembur = calendarLembur.get(Calendar.MONTH) + 1; 
			
			int yearParameter = calendar.get(Calendar.YEAR); // Mengambil tahun
			int monthParameter = calendar.get(Calendar.MONTH) + 1; 
			if(lemburdanbonus.getKaryawan().getIdKaryawan() == lemburdanbonusLoop.getKaryawan().getIdKaryawan()
					&& yearParameter == yearLembur && monthParameter == monthLembur ) {
				isExist = true;
				}
			
			
		}
		if(isExist) {
			System.out.println("error!");
			result.put("Status", 405);
			result.put("Error", "Duplikat Data Terdeteksi!");
		}else {
			saveLemburdanbonus = lemburdanbonusRepository.save(lemburdanbonus);
			result.put("Data", saveLemburdanbonus);
			result.put("Status", 200);
			result.put("Message", "Create data Lemburdanbonus success!");
			System.out.println("bulannya beda");
		}

		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	public List<LemburdanbonusDTO> MappingLemburdanbonus(){
		ModelMapper modelMapper = new ModelMapper();
		
		List<Lemburdanbonus> listLemburdanbonus= lemburdanbonusRepository.findAll();
		List<LemburdanbonusDTO> listLemburdanbonusDTO = new ArrayList<LemburdanbonusDTO>();
		for(Lemburdanbonus lemburdanbonus : listLemburdanbonus) {
			LemburdanbonusDTO lemburdanbonusDTO = modelMapper.map(lemburdanbonus, LemburdanbonusDTO.class);
			listLemburdanbonusDTO.add(lemburdanbonusDTO);
		}
		return listLemburdanbonusDTO;
	}
	public List<PendapatanDTO> MappingPendapatan(){
		ModelMapper modelMapper = new ModelMapper();
		
		List<Pendapatan> listPendapatan = pendapatanRepository.findAll();
		List<PendapatanDTO> listPendapatanDTO = new ArrayList<PendapatanDTO>();
		for(Pendapatan pendapatan : listPendapatan) {
			PendapatanDTO pendapatanDTO = modelMapper.map(pendapatan, PendapatanDTO.class);
			listPendapatanDTO.add(pendapatanDTO);
		}
		return listPendapatanDTO;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateLemburdanbonus(@Valid @RequestBody LemburdanbonusDTO lemburdanbonusDTO,
			@PathVariable(value = "id") Long idLemburdanbonus){

		Map<String, Object> result = new HashMap<String, Object>(); 
		ModelMapper modelMapper = new ModelMapper();
		Lemburdanbonus lemburdanbonusById = lemburdanbonusRepository.findById(idLemburdanbonus).get();
		lemburdanbonusById = modelMapper.map(lemburdanbonusDTO,Lemburdanbonus.class);
		
		lemburdanbonusById.setIdLemburdanbonus(idLemburdanbonus);
		
		lemburdanbonusRepository.save(lemburdanbonusById);
		result.put("Status", 200);
		result.put("Message", "Update data Lemburdanbonus success!");
		result.put("Data", lemburdanbonusById);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePublisherById(@PathVariable(value = "id") Long idLemburdanbonus) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Lemburdanbonus lemburdanbonus = lemburdanbonusRepository.findById(idLemburdanbonus).get();
    	
        lemburdanbonusRepository.delete(lemburdanbonus);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record category success!");
		result.put("ID : ",""+idLemburdanbonus);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
}
