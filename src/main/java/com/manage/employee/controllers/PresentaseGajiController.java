package com.manage.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manage.employee.dtomodel.PresentaseGajiDTO;
import com.manage.employee.model.PresentaseGaji;
import com.manage.employee.repositories.PersentasiGajiRepository;
@RestController
@RequestMapping("api/presentaseGaji")
public class PresentaseGajiController {
	@Autowired
	PersentasiGajiRepository presentaseGajiRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllPresentaseGaji(){
		
		ModelMapper modelMapper = new ModelMapper();
		Map<String, Object> result = new HashMap<String, Object>();
		List<PresentaseGaji> listPresentaseGaji= presentaseGajiRepository.findAll();
		List<PresentaseGajiDTO> listPresentaseGajiDTO = new ArrayList();
		
		for(PresentaseGaji presentaseGaji : listPresentaseGaji) {
			PresentaseGajiDTO dto = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
			listPresentaseGajiDTO.add(dto);
		}
			
		result.put("Status", 200);
		result.put("Message", "Read data PresentaseGaji success!");
		result.put("Data", listPresentaseGajiDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreatePresentaseGaji(@Valid @RequestBody PresentaseGajiDTO presentaseGajiDTO){
		ModelMapper modelMapper = new ModelMapper();
			PresentaseGaji presentaseGaji = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Create data PresentaseGaji success!");
		result.put("Data", presentaseGajiRepository.save(presentaseGaji));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updatePresentaseGaji(@Valid @RequestBody PresentaseGajiDTO presentaseGajiDTO,
			@PathVariable(value = "id") Long idPresentaseGaji){

		Map<String, Object> result = new HashMap<String, Object>(); 
		ModelMapper modelMapper = new ModelMapper();
		PresentaseGaji presentaseGajiById = presentaseGajiRepository.findById(idPresentaseGaji).get();
		presentaseGajiById = modelMapper.map(presentaseGajiDTO,PresentaseGaji.class);
		
		presentaseGajiById.setIdPresentaseGaji(idPresentaseGaji);
		
		presentaseGajiRepository.save(presentaseGajiById);
		result.put("Status", 200);
		result.put("Message", "Update data PresentaseGaji success!");
		result.put("Data", presentaseGajiById);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePublisherById(@PathVariable(value = "id") Long idPresentaseGaji) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	PresentaseGaji presentaseGaji = presentaseGajiRepository.findById(idPresentaseGaji).get();
    	
        presentaseGajiRepository.delete(presentaseGaji);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record category success!");
		result.put("ID : ",""+idPresentaseGaji);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
}
