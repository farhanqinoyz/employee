package com.manage.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manage.employee.dtomodel.KemampuanDTO;
import com.manage.employee.model.Kemampuan;
import com.manage.employee.repositories.KemampuanRepository;
@RestController
@RequestMapping("api/kemampuan")
public class KemampuanController {
	@Autowired
	KemampuanRepository kemampuanRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllKemampuan(){
		
		ModelMapper modelMapper = new ModelMapper();
		Map<String, Object> result = new HashMap<String, Object>();
		List<Kemampuan> listKemampuan= kemampuanRepository.findAll();
		List<KemampuanDTO> listKemampuanDTO = new ArrayList();
		
		for(Kemampuan kemampuan : listKemampuan) {
			KemampuanDTO dto = modelMapper.map(kemampuan, KemampuanDTO.class);
			listKemampuanDTO.add(dto);
		}
			
		result.put("Status", 200);
		result.put("Message", "Read data Kemampuan success!");
		result.put("Data", listKemampuanDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreateKemampuan(@Valid @RequestBody KemampuanDTO kemampuanDTO){
		ModelMapper modelMapper = new ModelMapper();
			Kemampuan kemampuan = modelMapper.map(kemampuanDTO, Kemampuan.class);
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Create data Kemampuan success!");
		result.put("Data", kemampuanRepository.save(kemampuan));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateKemampuan(@Valid @RequestBody KemampuanDTO kemampuanDTO,
			@PathVariable(value = "id") Long idKemampuan){

		Map<String, Object> result = new HashMap<String, Object>(); 
		ModelMapper modelMapper = new ModelMapper();
		Kemampuan kemampuanById = kemampuanRepository.findById(idKemampuan).get();
		kemampuanById = modelMapper.map(kemampuanDTO,Kemampuan.class);
		
		kemampuanById.setIdKemampuan(idKemampuan);
		
		kemampuanRepository.save(kemampuanById);
		result.put("Status", 200);
		result.put("Message", "Update data Kemampuan success!");
		result.put("Data", kemampuanById);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePublisherById(@PathVariable(value = "id") Long idKemampuan) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Kemampuan kemampuan = kemampuanRepository.findById(idKemampuan).get();
    	
        kemampuanRepository.delete(kemampuan);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record category success!");
		result.put("ID : ",""+idKemampuan);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
}
