package com.manage.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manage.employee.dtomodel.AgamaDTO;
import com.manage.employee.model.Agama;
import com.manage.employee.repositories.AgamaRepository;
@RestController
@RequestMapping("api/agama")
public class AgamaController {
	@Autowired
	AgamaRepository agamaRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllAgama(){
		
		ModelMapper modelMapper = new ModelMapper();
		Map<String, Object> result = new HashMap<String, Object>();
		List<Agama> listAgama= agamaRepository.findAll();
		List<AgamaDTO> listAgamaDTO = new ArrayList();
		
		for(Agama agama : listAgama) {
			AgamaDTO dto = modelMapper.map(agama, AgamaDTO.class);
			listAgamaDTO.add(dto);
		}
			
		result.put("Status", 200);
		result.put("Message", "Read data Agama success!");
		result.put("Data", listAgamaDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreateAgama(@Valid @RequestBody AgamaDTO agamaDTO){
		ModelMapper modelMapper = new ModelMapper();
			Agama agama = modelMapper.map(agamaDTO, Agama.class);
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Create data Agama success!");
		result.put("Data", agamaRepository.save(agama));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateAgama(@Valid @RequestBody AgamaDTO agamaDTO,
			@PathVariable(value = "id") Long idAgama){

		Map<String, Object> result = new HashMap<String, Object>(); 
		ModelMapper modelMapper = new ModelMapper();
		Agama agamaById = agamaRepository.findById(idAgama).get();
		agamaById = modelMapper.map(agamaDTO,Agama.class);
		
		agamaById.setIdAgama(idAgama);
		
		agamaRepository.save(agamaById);
		result.put("Status", 200);
		result.put("Message", "Update data Agama success!");
		result.put("Data", agamaById);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePublisherById(@PathVariable(value = "id") Long idAgama) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Agama agama = agamaRepository.findById(idAgama).get();
    	
        agamaRepository.delete(agama);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record category success!");
		result.put("ID : ",""+idAgama);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
}
