package com.manage.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manage.employee.dtomodel.PosisiDTO;
import com.manage.employee.model.Posisi;
import com.manage.employee.repositories.PosisiRepository;
@RestController
@RequestMapping("api/posisi")
public class PosisiController {
	@Autowired
	PosisiRepository posisiRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllPosisi(){
		
		ModelMapper modelMapper = new ModelMapper();
		Map<String, Object> result = new HashMap<String, Object>();
		List<Posisi> listPosisi= posisiRepository.findAll();
		List<PosisiDTO> listPosisiDTO = new ArrayList();
		
		for(Posisi posisi : listPosisi) {
			PosisiDTO dto = modelMapper.map(posisi, PosisiDTO.class);
			listPosisiDTO.add(dto);
		}
			
		result.put("Status", 200);
		result.put("Message", "Read data Posisi success!");
		result.put("Data", listPosisiDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreatePosisi(@Valid @RequestBody PosisiDTO posisiDTO){
		ModelMapper modelMapper = new ModelMapper();
			Posisi posisi = modelMapper.map(posisiDTO, Posisi.class);
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Create data Posisi success!");
		result.put("Data", posisiRepository.save(posisi));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updatePosisi(@Valid @RequestBody PosisiDTO posisiDTO,
			@PathVariable(value = "id") Long idPosisi){

		Map<String, Object> result = new HashMap<String, Object>(); 
		ModelMapper modelMapper = new ModelMapper();
		Posisi posisiById = posisiRepository.findById(idPosisi).get();
		posisiById = modelMapper.map(posisiDTO,Posisi.class);
		
		posisiById.setIdPosisi(idPosisi);
		
		posisiRepository.save(posisiById);
		result.put("Status", 200);
		result.put("Message", "Update data Posisi success!");
		result.put("Data", posisiById);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePublisherById(@PathVariable(value = "id") Long idPosisi) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Posisi posisi = posisiRepository.findById(idPosisi).get();
    	
        posisiRepository.delete(posisi);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record category success!");
		result.put("ID : ",""+idPosisi);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
}
