package com.manage.employee.dtomodel;

public class TingkatanDTO {
	
	private Long idTingkatan;
	private String namaTingkatan;
	public TingkatanDTO() {
		
	}
	public TingkatanDTO(Long idTingkatan, String namaTingkatan) {
		super();
		this.idTingkatan = idTingkatan;
		this.namaTingkatan = namaTingkatan;
	}
	public Long getIdTingkatan() {
		return idTingkatan;
	}
	public void setIdTingkatan(Long idTingkatan) {
		this.idTingkatan = idTingkatan;
	}
	public String getNamaTingkatan() {
		return namaTingkatan;
	}
	public void setNamaTingkatan(String namaTingkatan) {
		this.namaTingkatan = namaTingkatan;
	}
	
	
}
