package com.manage.employee.dtomodel;

import com.manage.employee.model.Karyawan;
import com.manage.employee.model.Kemampuan;

public class ListKemampuanDTO {
	private Long idListKemampuan;
	private Karyawan karyawan;
	private Kemampuan kemampuan;
	private Integer nilaiKemampuan;
	
	public ListKemampuanDTO(Long idListKemampuan, Karyawan karyawan, Kemampuan kemampuan, Integer nilaiKemampuan) {
		super();
		this.idListKemampuan = idListKemampuan;
		this.karyawan = karyawan;
		this.kemampuan = kemampuan;
		this.nilaiKemampuan = nilaiKemampuan;
	}
	public Long getIdListKemampuan() {
		return idListKemampuan;
	}
	public void setIdListKemampuan(Long idListKemampuan) {
		this.idListKemampuan = idListKemampuan;
	}
	public Karyawan getKaryawan() {
		return karyawan;
	}
	public void setKaryawan(Karyawan karyawan) {
		this.karyawan = karyawan;
	}
	public Kemampuan getKemampuan() {
		return kemampuan;
	}
	public void setKemampuan(Kemampuan kemampuan) {
		this.kemampuan = kemampuan;
	}
	public Integer getNilaiKemampuan() {
		return nilaiKemampuan;
	}
	public void setNilaiKemampuan(Integer nilaiKemampuan) {
		this.nilaiKemampuan = nilaiKemampuan;
	}
	
	
}
