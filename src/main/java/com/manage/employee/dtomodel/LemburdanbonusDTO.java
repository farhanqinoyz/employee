package com.manage.employee.dtomodel;

import java.util.Date;

import com.manage.employee.model.Karyawan;

public class LemburdanbonusDTO {
	private Long idLemburdanbonus;
	private Karyawan karyawan;
	private Integer variableBonus;
	private Integer lamaLembur;
	private Date tanggal;
	
	public LemburdanbonusDTO() {
		
	}

	public LemburdanbonusDTO(Long idLemburdanbonus, Karyawan karyawan, Integer variableBonus, Integer lamaLembur,
			Date tanggal) {
		super();
		this.idLemburdanbonus = idLemburdanbonus;
		this.karyawan = karyawan;
		this.variableBonus = variableBonus;
		this.lamaLembur = lamaLembur;
		this.tanggal = tanggal;
	}

	public Long getIdLemburdanbonus() {
		return idLemburdanbonus;
	}

	public void setIdLemburdanbonus(Long idLemburdanbonus) {
		this.idLemburdanbonus = idLemburdanbonus;
	}

	public Karyawan getKaryawan() {
		return karyawan;
	}

	public void setKaryawan(Karyawan karyawan) {
		this.karyawan = karyawan;
	}

	public Integer getVariableBonus() {
		return variableBonus;
	}

	public void setVariableBonus(Integer variableBonus) {
		this.variableBonus = variableBonus;
	}

	public Integer getLamaLembur() {
		return lamaLembur;
	}

	public void setLamaLembur(Integer lamaLembur) {
		this.lamaLembur = lamaLembur;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}
	
	
	
}
