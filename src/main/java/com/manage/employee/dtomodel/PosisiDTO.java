package com.manage.employee.dtomodel;

public class PosisiDTO {
	private Long idPosisi;
	private String namaPosisi;
	public PosisiDTO() {
		
	}
	public PosisiDTO(Long idPosisi, String namaPosisi) {
		super();
		this.idPosisi = idPosisi;
		this.namaPosisi = namaPosisi;
	}
	public Long getIdPosisi() {
		return idPosisi;
	}
	public void setIdPosisi(Long idPosisi) {
		this.idPosisi = idPosisi;
	}
	public String getNamaPosisi() {
		return namaPosisi;
	}
	public void setNamaPosisi(String namaPosisi) {
		this.namaPosisi = namaPosisi;
	}
	
	
}
