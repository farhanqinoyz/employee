package com.manage.employee.dtomodel;

public class AgamaDTO{
	private Long idAgama;
	private String namaAgama;
	
	public AgamaDTO() {
		
	}

	public AgamaDTO(Long idAgama, String namaAgama) {
		super();
		this.idAgama = idAgama;
		this.namaAgama = namaAgama;
	}

	public Long getIdAgama() {
		return idAgama;
	}

	public void setIdAgama(Long idAgama) {
		this.idAgama = idAgama;
	}

	public String getNamaAgama() {
		return namaAgama;
	}

	public void setNamaAgama(String namaAgama) {
		this.namaAgama = namaAgama;
	}
	
	
}
