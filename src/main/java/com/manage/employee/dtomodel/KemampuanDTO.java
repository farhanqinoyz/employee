package com.manage.employee.dtomodel;

import com.manage.employee.model.KategoriKemampuan;

public class KemampuanDTO {
	private Long idKemampuan;
	private KategoriKemampuan kategoriKemampuan;
	private String namaKemampuan;
	public KemampuanDTO(Long idKemampuan, KategoriKemampuan kategoriKemampuan, String namaKemampuan) {
		super();
		this.idKemampuan = idKemampuan;
		this.kategoriKemampuan = kategoriKemampuan;
		this.namaKemampuan = namaKemampuan;
	}
	public Long getIdKemampuan() {
		return idKemampuan;
	}
	public void setIdKemampuan(Long idKemampuan) {
		this.idKemampuan = idKemampuan;
	}
	public KategoriKemampuan getKategoriKemampuan() {
		return kategoriKemampuan;
	}
	public void setKategoriKemampuan(KategoriKemampuan kategoriKemampuan) {
		this.kategoriKemampuan = kategoriKemampuan;
	}
	public String getNamaKemampuan() {
		return namaKemampuan;
	}
	public void setNamaKemampuan(String namaKemampuan) {
		this.namaKemampuan = namaKemampuan;
	}
	
	
}
